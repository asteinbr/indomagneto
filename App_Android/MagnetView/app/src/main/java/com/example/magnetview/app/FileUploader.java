package com.example.magnetview.app;

import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alexander on 26.05.2014.
 */
public class FileUploader {
    private static String uploadServerURI = "http://146.185.179.179/upload.php";
    private static int serverResponseCode;

    private FileUploader() {
        // Suppress default constructor
        throw new AssertionError();
    }

    public static int uploadFile(String fileUrl) {
        String filename = fileUrl;

        HttpURLConnection connection = null;

        String eof = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(fileUrl);

        if (!sourceFile.isFile()) {
            Log.e("FileUploader", "The given File URI is not valid: " + sourceFile.toString());
            //System.out.println("The given file uri is not valid: " + sourceFile.toString());

            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(uploadServerURI);

                // open connection
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("uploaded_file", filename);

                DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + eof);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";" + " filename=\"" + filename + "\"" + eof);
                dos.writeBytes(eof);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(eof);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + eof);

                // Responses from the server (code and message)
                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "+ serverResponseMessage + ": " + serverResponseCode);
                //System.out.println("HTTP Response is : "+ serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    Log.i("FileUploader", "File Upload complete.");
                //    System.out.println("File upload complete!");
                }

                // close
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (Exception e) {
                Log.e("FileUploader", "Error during uploading file to remote server: " + e.getMessage());
            }

            return serverResponseCode;
        }
    }

//    public static void main(String args[] ) {
//        File file = new File("testfile.txt");
//        uploadFile(file.getAbsolutePath());
//    }

}
