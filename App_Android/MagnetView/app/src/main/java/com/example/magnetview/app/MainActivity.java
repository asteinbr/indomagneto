package com.example.magnetview.app;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity implements SensorEventListener {
    private boolean D = false;
    private boolean D_logSensorData = false;
    private SensorManager mSensorManager;
    private Sensor mSensorMagneticField;
    private Sensor mSensorGyro;
    private Sensor mSensorAccel;
    private boolean isSensorActivated = false;
    private boolean isFileStreamOpen = false;
    private FileOutputStream outputStream;
    private String filename;
    String magnetData_X;
    String magnetData_Y;
    String magnetData_Z;
    String gyroData_X;
    String gyroData_Y;
    String gyroData_Z;
    String accelData_X;
    String accelData_Y;
    String accelData_Z;
    String orientationData_Azimuth;
    String orientationData_Pitch;
    String orientationData_Roll;
    private String FILENAME_TYPE = ".txt";
    DecimalFormat formatOrientation = new DecimalFormat("#");
    DecimalFormat formatAccelGyro = new DecimalFormat("#.##");
    DecimalFormat formatMagnetGyro = new DecimalFormat("#.##");

    private String lastItem = "NEWFILE";

    static final float ALPHA = 0.25f;

    Message msg;
    Handler handler;

    // changes for sensor fusioning
    // orientation angles from accel and magnet
    private float[] accMagOrientation = new float[3];
    // accelerometer and magnetometer based rotation matrix
    private float[] rotationMatrix = new float[9];
    private float[] magnetDataArray = new float[3];
    private float[] accelDataArray = new float[3];
    private float[] gyroDataArray = new float[3];
    private boolean isLowPassFilterEnabled = false;
    private boolean isStarted;


    private void intializeArrays() {
        accMagOrientation = new float[3];
        rotationMatrix = new float[9];
        magnetDataArray = new float[3];
        accelDataArray = new float[3];
        gyroDataArray = new float[3];
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("onCreate()", "called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set text
        setMathText();

        // init arrays
        intializeArrays();

        // init handler
        startHandler();

        // init sensors
        initializeSensorManager();

        final Button button_UseCurrentDate = (Button) findViewById(R.id.button_UseCurrentDate);
        button_UseCurrentDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /*Date date = new Date();
                Timestamp timestamp = new Timestamp(date.getTime());
                Log.i("onButtonCLick", timestamp.toString());

                try {
                    EditText editText_Name = (EditText) findViewById(R.id.editText_Name);
                    editText_Name.setMathText(timestamp.toString());
                } catch (Exception e) {
                    String msg = (e.getMessage() == null) ? "btnCurDate: Something is went wrong." : e.getMessage();
                    Log.e("onButton_Name", msg);
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        final Button button_Start = (Button) findViewById(R.id.button_Start);
        button_Start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    EditText editText_Name = (EditText) findViewById(R.id.editText_Name);
                    filename = editText_Name.getText().toString() + FILENAME_TYPE;
                    startDataCollection();
                    isStarted = true;
                } catch (Exception e) {
                    String msg = (e.getMessage() == null) ? "btnStart: Something is went wrong." : e.getMessage();
                    Log.e("onButton_Start", msg);
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            }
        });

        final Button button_Stop = (Button) findViewById(R.id.button_Stop);
        button_Stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    // thread is checking for correct end of the current file, the last line has to be "GYROSCOPE,..."
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            if (lastItem.contains("GYROSCOPE")) {
                                stopDataCollection();
                                isStarted = false;
                                this.cancel();
                            }
                        }
                    }, 0, 10);
                } catch (Exception e) {
                    String msg = (e.getMessage() == null) ? "btnStop: Something is went wrong." : e.getMessage();
                    Log.e("onButton_Start", msg);
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onLowPassFilterToggleClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();

        if (on)
            isLowPassFilterEnabled = true;
        else
            isLowPassFilterEnabled = false;
    }

    private void initializeSensorManager() {
        Log.i("initializeSensorManager()", "called");

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorMagneticField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSensorGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mSensorAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    private void registerSensors() {
        mSensorManager.registerListener(this, mSensorMagneticField, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorGyro, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mSensorAccel, SensorManager.SENSOR_DELAY_NORMAL);

    }

    private void startDataCollection() {
        Log.i("startDataCollection()", "called");

        // init arrays
        intializeArrays();

        createFileAndFileStream(filename);
        // create file on local fs and write sensor data to it
        // write results of sensors into the file
        if (!isSensorActivated) {
            Log.i("startDataCollection()", "!isSensorActivated");
            registerSensors();
            isSensorActivated = true;
        }
    }

    private void startHandler() {
        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                handler.obtainMessage();
                if (msg.arg1 == 1)
                    Toast.makeText(getApplicationContext(), "Upload successful.", Toast.LENGTH_SHORT).show();
                else if (msg.arg1 == 2)
                    Toast.makeText(getApplicationContext(), "Upload NOT successful.", Toast.LENGTH_SHORT).show();

                return false;
            }
        });
    }

    private void stopDataCollection() {
        Log.i("stopDataCollection()", "called");

        if (isStarted) {
            mSensorManager.unregisterListener(this);
            isSensorActivated = false;

            closeFileStream();
        }
    }

    public void onResume() {
        Log.i("onResume()", "called");
        super.onResume();
    }

    public void onPause() {
        Log.i("onPause()", "called");
        super.onPause();

        if (isSensorActivated)
            mSensorManager.unregisterListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i("onCreateOptionsMenu()", "called");

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i("onOptionsItemSelected()", "called");

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (D) Log.i("onSensorChanged()", "called");
        Sensor sensor = sensorEvent.sensor;

        if (sensorEvent.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            Toast.makeText(getApplicationContext(), "Please calibrate the Sensors now.", Toast.LENGTH_SHORT).show();
        }

        if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            if (isLowPassFilterEnabled)
                lowPassFilter(sensorEvent.values.clone(), magnetDataArray);
            else
                magnetDataArray = sensorEvent.values;

            magnetData_X = String.valueOf(formatMagnetGyro.format(magnetDataArray[0]));
            magnetData_Y = String.valueOf(formatMagnetGyro.format(magnetDataArray[1]));
            magnetData_Z = String.valueOf(formatMagnetGyro.format(magnetDataArray[2]));

            String magnetData = magnetDataArray[0] + "," + magnetDataArray[1] + "," + magnetDataArray[2] + "\n";
            if (D_logSensorData) Log.i("MAGNETIC_FIELD_", magnetData);
            writeToInternalFile("MAGNETIC_FIELD," + magnetData);

            TextView magnetX = (TextView) findViewById(R.id.textView_magnetDataX);
            magnetX.setText(magnetData_X);
            TextView magnetY = (TextView) findViewById(R.id.textView_magnetDataY);
            magnetY.setText(magnetData_Y);
            TextView magnetZ = (TextView) findViewById(R.id.textView_magnetDataZ);
            magnetZ.setText(magnetData_Z);

            TextView magnetMagnitudeXYZ = (TextView) findViewById(R.id.textView_magnetMagnitudeXYZ);
            float magnetMagnitudeValueXYZ = (float) Math.sqrt((magnetDataArray[0]) * (magnetDataArray[0]) +
                    (magnetDataArray[1]) * (magnetDataArray[1]) +
                    (magnetDataArray[2]) * (magnetDataArray[2]));
            magnetMagnitudeXYZ.setText(String.valueOf(formatMagnetGyro.format(magnetMagnitudeValueXYZ)));
            //writeToInternalFile("magnetMagnitudeXYZ," + magnetMagnitudeValueXYZ + "\n");

            TextView magnetMagnitudeXY = (TextView) findViewById(R.id.textView_magnetMagnitudeXY);
            float magnetMagnitudeValueXY = (float) Math.sqrt( (magnetDataArray[0])*(magnetDataArray[0]) +
                    (magnetDataArray[1])*(magnetDataArray[1]) );
            magnetMagnitudeXY.setText(String.valueOf(formatMagnetGyro.format(magnetMagnitudeValueXY)));
            //writeToInternalFile("magnetMagnitudeXY," + magnetMagnitudeValueXY + "\n");

            TextView magnetTheta = (TextView) findViewById(R.id.textView_magnetThetaContent);
            float magnetThetaValue = (float) Math.atan(magnetDataArray[2] / (Math.sqrt( (magnetDataArray[0])*(magnetDataArray[0]) + (magnetDataArray[1])*(magnetDataArray[1]))) );
            magnetTheta.setText(String.valueOf(formatMagnetGyro.format(magnetThetaValue)));
            //writeToInternalFile("magnetTheta," + magnetThetaValue + "\n");

            // cos((pi/2)-Theta)
            // magnet = magnetDataArray[i]
            // accel = accelDataArray[i]
            double cosPi2ThetaValue = ( accelDataArray[0]*magnetDataArray[0] + accelDataArray[1]*magnetDataArray[1] + accelDataArray[2]*magnetDataArray[2] ) /
                    ( Math.sqrt( (accelDataArray[0]*accelDataArray[0]) + (accelDataArray[1]*accelDataArray[1]) + (accelDataArray[2]*accelDataArray[2]) ) * Math.sqrt( (magnetDataArray[0]*magnetDataArray[0]) + (magnetDataArray[1]*magnetDataArray[1]) + (magnetDataArray[2]*magnetDataArray[2]) ) );
            TextView cosPi2ThetaResult = (TextView) findViewById(R.id.textView_cosTheta);
            cosPi2ThetaResult.setText(String.valueOf(cosPi2ThetaValue));
            //writeToInternalFile("cosPi2Theta," + cosPi2ThetaValue + "\n");

        } else if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            gyroDataArray = sensorEvent.values;

            gyroData_X = String.valueOf(formatAccelGyro.format(gyroDataArray[0]));
            gyroData_Y = String.valueOf(formatAccelGyro.format(gyroDataArray[1]));
            gyroData_Z = String.valueOf(formatAccelGyro.format(gyroDataArray[2]));

            String gyroData = gyroDataArray[0] + "," + gyroDataArray[1] + "," + gyroDataArray[2] + "\n";
            if (D_logSensorData) Log.i("GYROSCOPE", gyroData);
            writeToInternalFile("GYROSCOPE," + gyroData);

            TextView gyroX = (TextView) findViewById(R.id.textView_gyroDataX);
            gyroX.setText(gyroData_X);
            TextView gyroY = (TextView) findViewById(R.id.textView_gyroDataY);
            gyroY.setText(gyroData_Y);
            TextView gyroZ = (TextView) findViewById(R.id.textView_gyroDataZ);
            gyroZ.setText(gyroData_Z);
        } else if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (isLowPassFilterEnabled)
                lowPassFilter(sensorEvent.values.clone(), accelDataArray);
            else
                accelDataArray = sensorEvent.values;

            calculateAccMagOrientation();

            accelData_X = String.valueOf(formatAccelGyro.format(accelDataArray[0]));
            accelData_Y = String.valueOf(formatAccelGyro.format(accelDataArray[1]));
            accelData_Z = String.valueOf(formatAccelGyro.format(accelDataArray[2]));

            String accelData = accelDataArray[0] + "," + accelDataArray[1] + "," + accelDataArray[2] + "\n";
            if (D_logSensorData) Log.i("ACCELEROMETER", accelData);
            writeToInternalFile("ACCELEROMETER," + accelData);

            TextView accelX = (TextView) findViewById(R.id.textView_accelDataX);
            accelX.setText(accelData_X);
            TextView accelY = (TextView) findViewById(R.id.textView_accelDataY);
            accelY.setText(accelData_Y);
            TextView accelZ = (TextView) findViewById(R.id.textView_accelDataZ);
            accelZ.setText(accelData_Z);

            TextView accelMagnitudeXYZ = (TextView) findViewById(R.id.textView_accelMagnitudeXYZ);
            float accelMagnitudeValueXYZ = (float) Math.sqrt((accelDataArray[0]) * (accelDataArray[0]) +
                    (accelDataArray[1]) * (accelDataArray[1]) +
                    (accelDataArray[2]) * (accelDataArray[2]));
            accelMagnitudeXYZ.setText(String.valueOf(formatAccelGyro.format(accelMagnitudeValueXYZ)));
            //writeToInternalFile("accelMagnitudeXYZ," + accelMagnitudeValueXYZ + "\n");

            TextView accelMagnitudeXY = (TextView) findViewById(R.id.textView_accelMagnitudeXY);
            float accelProductValueXY = (float) Math.sqrt( (accelDataArray[0])*(accelDataArray[0]) +
                    (accelDataArray[1])*(accelDataArray[1]) );
            accelMagnitudeXY.setText(String.valueOf(formatAccelGyro.format(accelProductValueXY)));
            //writeToInternalFile("accelMagnitudeXY," + accelProductValueXY + "\n");

            TextView accelTheta = (TextView) findViewById(R.id.textView_accelThetaContent);
            float accelThetaValue = (float) Math.atan(accelDataArray[2] / (Math.sqrt( (accelDataArray[0])*(accelDataArray[0]) + (accelDataArray[1])*(accelDataArray[1]))) );
            accelTheta.setText(String.valueOf(formatMagnetGyro.format(accelThetaValue)));
            //writeToInternalFile("accelTheta," + accelThetaValue + "\n");

        }
    }

    protected float[] lowPassFilter(float[] input, float[] output) {
        System.out.println("lowPassFilter()...");
        if (output == null)
            return input;

        for (int i = 0; i < input.length; i++) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }

        return output;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        Log.i("onAccuracyChanged()", "called");
    }

    public void calculateAccMagOrientation() {
        if (accelDataArray != null && magnetDataArray != null) {
            if (SensorManager.getRotationMatrix(rotationMatrix, null, accelDataArray, magnetDataArray)) {
                //SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, rotationMatrix);
                SensorManager.getOrientation(rotationMatrix, accMagOrientation);
            }

            accMagOrientation[0] = (float) Math.toDegrees(accMagOrientation[0]);
            accMagOrientation[1] = (float) Math.toDegrees(accMagOrientation[1]);
            accMagOrientation[2] = (float) Math.toDegrees(accMagOrientation[2]);

            if (accMagOrientation[0] < 0) {
                accMagOrientation[0] += 360;
            }
            if (accMagOrientation[1] < 0) {
                accMagOrientation[1] += 360;
            }
            if (accMagOrientation[2] < 0) {
                accMagOrientation[2] += 360;
            }

            String orientationData = formatOrientation.format(accMagOrientation[0]) + "," + formatOrientation.format(accMagOrientation[1]) + "," + formatOrientation.format(accMagOrientation[2]) + "\n";
            if (D_logSensorData) Log.i("ORIENTATION", orientationData);
            writeToInternalFile("ORIENTATION," + orientationData);

            orientationData_Azimuth = String.valueOf(formatOrientation.format(accMagOrientation[0]) + "°");
            orientationData_Pitch = String.valueOf(formatOrientation.format(accMagOrientation[1]) + "°");
            orientationData_Roll = String.valueOf(formatOrientation.format(accMagOrientation[2]) + "°");

            TextView orientationAzimuth = (TextView) findViewById(R.id.textView_orientationDataAzimuth);
            orientationAzimuth.setText(orientationData_Azimuth);
            TextView orientationPitch = (TextView) findViewById(R.id.textView_orientationDataPitch);
            orientationPitch.setText(orientationData_Pitch);
            TextView orientationRoll = (TextView) findViewById(R.id.textView_orientationDataRoll);
            orientationRoll.setText(orientationData_Roll);
        }
    }

    private void createFileAndFileStream(String filename) {
        try {
            File file = new File(getApplicationContext().getFilesDir(), filename);
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            isFileStreamOpen = true;

            if (D) Log.i("_______________", getApplicationContext().getFilesDir().toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void writeToInternalFile(String message) {
        try {
            if (lastItem.contains("NEWFILE")) {
                if (message.contains("ORIENTATION")) {
                    outputStream.write(message.getBytes());
                    lastItem = message;
                }
            }
            if (lastItem.contains("ORIENTATION")) {
                if (message.contains("ACCELEROMETER")) {
                    outputStream.write(message.getBytes());
                    lastItem = message;
                }
            }
            if (lastItem.contains("ACCELEROMETER")) {
                if (message.contains("MAGNETIC_FIELD")) {
                    outputStream.write(message.getBytes());
                    lastItem = message;
                }
            }
            if (lastItem.contains("MAGNETIC_FIELD")) {
                if (message.contains("GYROSCOPE")) {
                    outputStream.write(message.getBytes());
                    lastItem = message;
                }
            }
            if (lastItem.contains("GYROSCOPE")) {
                if (message.contains("ORIENTATION")) {
                    outputStream.write(message.getBytes());
                    lastItem = message;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeFileStream() {
        if (isFileStreamOpen) {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!filename.isEmpty()) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    msg  = new Message();
                    if (FileUploader.uploadFile(getApplicationContext().getFilesDir() + "/" + filename) == 200)
                        msg.arg1 = 1;
                    else
                        msg.arg1 = 2;

                    handler.sendMessage(msg);
                }
            }).start();
        } else {
            Toast.makeText(getApplicationContext(), "Filename is empty, no File Upload possible.", Toast.LENGTH_SHORT).show();
        }
    }

    private void setMathText() {
        // Magnet
        String s_magnetMagnitudeXYZ = "&#8739;M<sub>xyz</sub>&#8739;:";
        TextView tv_magnetMagnitudeXYZ = (TextView) findViewById(R.id.textView_magnetMagnitudeXYZLabel);
        tv_magnetMagnitudeXYZ.setText(Html.fromHtml(s_magnetMagnitudeXYZ));

        String s_magnetMagnitudeXY = "&#8739;M<sub>xy</sub>&#8739;:";
        TextView tv_magnetMagnitudeXY = (TextView) findViewById(R.id.textView_magnetMagnitudeXYLabel);
        tv_magnetMagnitudeXY.setText(Html.fromHtml(s_magnetMagnitudeXY));

        String s_magnetTheta = "&Theta:";
        TextView tv_magnetTheta = (TextView) findViewById(R.id.textView_magnetThetaLabel);
        tv_magnetTheta.setText(Html.fromHtml(s_magnetTheta));

        // Accel
        String s_accelMagnitudeXYZ = "&#8739;A<sub>xyz</sub>&#8739;:";
        TextView tv_accelMagnitudeXYZ = (TextView) findViewById(R.id.textView_accelMagnitudeXYZLabel);
        tv_accelMagnitudeXYZ.setText(Html.fromHtml(s_accelMagnitudeXYZ));

        String s_accelMagnitudeXY = "&#8739;A<sub>xy</sub>&#8739;:";
        TextView tv_accelMagnitudeXY = (TextView) findViewById(R.id.textView_accelMagnitudeXYLabel);
        tv_accelMagnitudeXY.setText(Html.fromHtml(s_accelMagnitudeXY));

        String s_accelTheta = "&Theta:";
        TextView tv_accelTheta = (TextView) findViewById(R.id.textView_accelThetaLabel);
        tv_accelTheta.setText(Html.fromHtml(s_accelTheta));
    }
}
