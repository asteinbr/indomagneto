__author__ = 'asteinbr'

import os

def cleanUp(filename):
    with open(filename) as f:
        content = f.readlines()
    f.close()

    newContent = []

    for line in content:
        if "ORIENTATION" in line:
            newContent.append(line)
        if "ACCELEROMETER" in line:
            newContent.append(line)
        if "MAGNETIC_FIELD" in line:
            newContent.append(line)
        if "GYROSCOPE" in line:
            newContent.append(line)

    newFile = open(filename,"w")
    for item in newContent:
      newFile.write("%s" % item)
    newFile.close()

def getFilesFromDirectory(directory):
    files = []
    for file in os.listdir(directory):
        if file.endswith(".txt"):
            files.append(file)

    return files

def runCleaning(directory):
    files = getFilesFromDirectory(directory)

    for file in files:
        file = directory + file
        cleanUp(file)

############################

runCleaning("../IndoorLocalizationData/FHFFM/")
runCleaning("../IndoorLocalizationData/Soldatenkamp4/")