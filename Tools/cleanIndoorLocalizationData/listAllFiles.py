__author__ = 'Alexander'

import os,re

path = " <- getFile(\"http://146.185.179.179/uploads/IndoorLocalizationData/FHFFM/"

def getFilesFromDirectory(directory):
    files = []
    for file in os.listdir(directory):
        if file.endswith(".txt"):
            files.append(file)

    return files


def knownRooms():
    files = []
    for i in getFilesFromDirectory("../IndoorLocalizationData/FHFFM/"):
        if re.match("^\w\d{3}_\d*.txt", i):
            classifier = re.sub("_.*", "", i)
            classifier = re.sub("^\w", "", classifier)
            files.append(i.replace(".txt", ""))
            print(i.replace(".txt", "") + path + i + "\", \"" + classifier + "\", TRUE)")

    print("\nallFiles <- rbind(", end="")
    for i in files:
        print(i + ", ", end="")
    print(")")


def unknownRooms():
    files = []
    for i in getFilesFromDirectory("../IndoorLocalizationData/FHFFM/"):
        if re.match("^\w\d{3}-\d*.txt", i):
            classifier = re.sub("-.*", "", i)
            classifier = re.sub("^\w", "", classifier)
            files.append(i.replace(".txt", "").replace("-", "__"))
            print(i.replace(".txt", "").replace("-", "__") + path + i + "\", \"" + classifier + "\", TRUE)")

    print("\nallFiles <- rbind(", end="")
    for i in files:
        print(i + ", ", end="")
    print(")")

knownRooms()
unknownRooms()