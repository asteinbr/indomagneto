\chapter{Introduction}
\label{chap:Introduction}

\section{Overview}
\label{sec:Overview}
Since a few years Global Positioning Systems (GPS) provide localization, positioning and navigation services in outdoor environments for users and even machines. The accuracy of GPS is stated with a worst case of 7.8 meters within a confidence interval of 95\%, more accurate GPS modules are able to provide an accuracy about 3.5 meters \cite{GPSgov}. However, GPS is not working in indoor environments when no free sight to the satellites is available. Due to this reason other localization or positioning systems have to been discovered to get an accurate indoor position. Indoor positioning is important for navigation of robots within a building but also through the upturn of mobile devices like smartphones, with their built-in sensors, new fields of application have been established where indoor localization can be applied for and become rigorously important.

There exist different ways and approaches for indoor self localization. Based on the highest level the different approaches can be divided into three physical principles \cite{Mautz2012}:
\begin{itemize}
	\item \textbf{Inertial Navigation}:\\
	computer with sensors like gyroscope and accelerometer which are maintaining angular momentum
	\item \textbf{Mechanical waves}:\\
	audible or ultra-sound, waves on water surface
	\item \textbf{Electromagnetic waves}:\\
	visible, infrared, microwave or radio spectrum; used in radars, cellular networks, WiFi or Beacons
\end{itemize}
One method for indoor self localization is based on ambient magnetic fields, which are everywhere on the earth because they are created by the inner core of our planet. The magnetic field can affect objects like metal, walls or electronic devices which can be found in buildings \cite{Haverinen2009}. These objects create interferences on the local magnetic field and result in different patterns within the magnetic field values in buildings \cite{Yamazaki2003}. This pattern can be used, after an initial learning or training phase, to navigate or locate the user indoors. The pattern matching can be realized with machine learning algorithms.

\section{Related Work}
\label{sec:Related Work}
Several attempts have been performed in order to create or test models for navigation or localization with utilization of the magnetic field.

\cite{Storms2010} describes a lead-follower scenario where a robot, i.e. the leader, is driving across an indoor floor and sending the magnetic field data to a second robot, i.e. the follower, which is trying to follow the leader just with assistance of the received magnetic field data. The magnetic data is being processed by a \emph{Kalman Filter}. Kalman Filter (KF) are forward recursive data processing algorithms based on prediction and measurement methods \cite{Bishop2007}. An example for often used applications for KF is the tracking of moving objects, it is useful for online data, where the data is streamed continuously \cite{Murphy201208}, see also section \ref{subs:On-Line Learning}. They are able to remove noise out of measurements. The accomplished accuracy of this method is about approximately 60 cm.

\cite{Vallivaara2010} proposed an approach for simultaneous localization and mapping (SLAM) method based on the ambient magnetic field. SLAM is a method where no map data is known, the robot in this case, drives indoors and acquires magnet field data from the magnetometer sensors and localizes itself based on the data. It is like the unsupervised learning approach which is described later in section \ref{subs:Unsupervised Learning}. In order to model the magnetic field map Gaussian Process regression is being used. A \emph{Rao-Blackwellized Particle Filter} (RBPF) was used to estimate the position distribution of the robot. RBPF is a special form of Particle Filters (PF). PF generalizes KF methods \cite{Arulampalam2002}. There algorithms are used for state estimations, e.g. to predict the position of aircrafts. However, PF have problems with growing state dimensions \cite{SchoenRBPF}. RBPF is a modified version of PF, it is a combination of KF and PF. For each particle is a KF associated. The team showed that the positioning estimation is working with these techniques and with low-cost magnetometers. However, the experiment was performed in a small apartment.

\cite{Li2012} are experimenting with main focus on fingerprints of the magnetic field data. The indoor map with magnetic field data was recorded in blocks which are 30.5 x 30.5 cm. This approach doesn't use stateful techniques like KF or PF, instead of them a basic nearest neighbors algorithm (NN) was used. The results showed that an accuracy in decimeter level was possible with three magnetic field values and the fingerprint-function. Additionally, the team discovered that different magnetometer sensors in smartphones result also in a different outcome. No tests were done with different heights of the magnetometer sensor, including heights could lead to different outcomes. Thus, the results are more suitable for laboratory like environments.

Quite similar to \cite{Storms2010} the researcher from \cite{Haverinen2009} tried the approach of one- dimensional localization within corridors of hospitals and libraries for robots and humans. The used method was Monte Carlo Localization which is known as Particle Filter. Based on the idea of terrain navigation of autonomous underwater vehicles (AUV), e.g. torpedo's, \cite{Tyren1987,Karlsson2003,Nygren2004}, the team proposed a working solution with a maximum error of 28 cm for localization. But the robot needs about 25 meters to get an accurate self-position. Also the results were stable for longer periods which indicates that the magnetic field is stable for longer time periods. The research work was contributed to IndoorAtlas, which commercialize this process for customers with smartphones \cite{haverinen2014generating,haverinen2014utilizing}.

Besides of the magnetic field based localization, also different methods may apply for location of users or other objects as mentioned in section \ref{sec:Overview}. For example \cite{Schaefer2014} proposed a fingerprinting approach based on properties like signal strength of WiFi access points in a realistic and non-laboratory like environment with smartphones. The proposed approach uses Support Vector Machines, k-Nearest-Neighbors and Na\"ive Bayes machine learning algorithms to predict the location of the user or object. The focus lies on the prediction of rooms, not navigation through a corridor or floor. The achieved solution was able to deliver the correct room with a Fmeasure of $f = 0.985$ for two mobile-devices respectively $f = 0.967$ with three devices.

\section{Motivation}
\label{sec:Motivation}
Research by Boles and Lohmann \cite{Boles2003} and even by Mouritsen et. al. \cite{Mouritsen2004} showed that animals uses the earth's magnetic field for navigate themselves.

For example \cite{Boles2003} discovered during a research that spiny lobsters are able to relocate themselves. The lobsters were collected around the Florida Keyes and were relocated about 12 to 37 km from their originate position. The lobsters were able to get back to their origin on their own. This shows, that spiny lobsters are able to use the magnetic field to orientate themselves.

Next to the lobsters, the team around \cite{Mouritsen2004} showed for the case of migratory birds, how they use the magnetic field in order to find the correct way to the north or the south. The research experiments showed how the birds are reacting in natural-geomagnetic field and in zero-magnetic fields. While the birds are showing the head in the appropriate direction when they are in a natural-geomagnetic field, the head randomly moves in a zero-geomagnetic field. This experiment shows even that the magnetic sensory organ is located in the head of the birds.

The idea, that even animals are able to use the magnetic field for navigation or positioning, consults researches to use this approach. An example is indoor navigation. Where the ambient magnetic field is influenced by ferro metals, walls, electrical wires and other objects and thereby patterns within the magnetic data set can be observed.

Most of the approaches use stateful techniques to navigate or locate via magnetic field, classification tasks are rare. Also most papers which are offered on IEEE\footnote{\url{http://ieeexplore.ieee.org}} and ACM\footnote{\url{http://dl.acm.org}} are using navigation through indoor corridors. However, one publication was found that is using localization of rooms, in fact based on WiFi fingerprinting \cite{Schaefer2014} and classification tasks. The question came up, if this approach can be transported and realized with magnetic field vectors and with help of machine learning classification algorithms.

Thus, this thesis evolved. The aim is to find a solution that is able to get the correct room of a set of rooms with assistance of magnetic field data acquired by a built-in magnetometer sensor of a smartphone. Although the magnetic field data is being processed by popular machine learning algorithms based on classification tasks.
%
%Besides of this, the question arise, if it is possible to combine two different learner methods like the WiFi method from \cite{Schaefer2014} with the magnetic field data of this thesis, to establish a vote-like approach to improve the WiFi results, which are already quite well.

\section{Outline}
\label{sec:Outline}
After this introductory chapter \ref{chap:Introduction} an overview of the magnetic field of the earth will follow in chapter \ref{chap:Magnetic Field}. In chapter \ref{chap:Machine Learning} an overview of machine learning is described. Afterwards, in chapter \ref{chap:Machine Learning Algorithms}, the three machine learning algorithms, which are used later during the thesis, are described. Chapter \ref{chap:Architecture / Implementation} describes the actual architecture and implementation of the prototype including the results. Finally in chapter \ref{chap:Conclusion} the conclusion about this work is presented.
